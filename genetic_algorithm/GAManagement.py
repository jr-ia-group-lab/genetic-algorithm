import logging
import math
import random
from collections import OrderedDict
import matplotlib.pyplot as plt

logging.basicConfig(level=logging.ERROR)

selection_method_dict = {'tournament': 1, 'ordered_best_score': 2, 'random_best_score': 3}
crossover_method_dict = {'single_point': 1, 'double_point': 2}


class GaManagement:
    def __init__(self, selection_tx, mutation_tx, stop_condition):
        if selection_tx > 0.5:
            raise ValueError('selection_tx must be lower or equal to 0.5')
        if mutation_tx > 1:
            raise ValueError('mutation_tx must be lower or equal to 1')

        self.population = []
        self.population_size = 0
        self.store_average_fitness = []
        self.store_best_score = []
        self.store_population_size = []
        self.population_fitness = None
        self.selection_tx = selection_tx
        self.mutation_tx = mutation_tx
        self.stop_condition = stop_condition

    def initial_population(self, population_list):
        self.population = population_list
        self.population_size = len(population_list)
        self.store_population_size.append(self.population_size)

    def calculate_fitness_population(self):
        pop_fitness = {}
        for i, individu in enumerate(self.population):
            pop_fitness.update({individu: individu.fitness()})
        self.population_fitness = OrderedDict(sorted(pop_fitness.items(), key=lambda x: x[1], reverse=True))
        self.store_average_fitness.append(
            sum(list(self.population_fitness.values())) / float(len(list(self.population_fitness.values()))))
        self.store_best_score.append(list(self.population_fitness)[0].score())
        return self.population_fitness

    def select_population(self, selection_method, elitism=0, tournament=0):
        # calculate to get an even number of population
        even_num = math.ceil((self.selection_tx * self.population_size) / 2.) * 2
        logging.info('number of population to select=', even_num)
        if elitism + 2 * tournament > even_num:
            raise ValueError('sum of values doesnt work even_num=' + str(even_num) + ' < elitism+(2*tournament)=' + str(
                elitism + 2 * tournament))
        if elitism > 0 and selection_method != 2:
            elitism_population = list(self.population_fitness)[:elitism]
            logging.info('number of elistism population =', len(elitism_population))
            even_num = even_num - elitism
            other_population = list(self.population_fitness)[elitism:]
            other_population_fitness = list(self.population_fitness.values())[elitism:]
            logging.info('number of elistism other population =', len(other_population))
            logging.info('number of rest of population to select after elitism=', even_num)

        else:
            elitism_population = []
            other_population = list(self.population_fitness)
            other_population_fitness = list(self.population_fitness.values())
            logging.info('number of other population =', len(other_population))

        if selection_method == 1 and tournament > 0:  # tournament method
            tournament_individuals = random.sample(other_population, tournament)
            logging.info('number of tournament population =', len(tournament_individuals))
            tournament_individuals_opponents = [k for k in other_population if
                                                k not in tournament_individuals]
            tournament_opponents = random.sample(tournament_individuals_opponents, tournament)
            logging.info('number of tournament opponents =', len(tournament_individuals))
            rest_of_the_population = [k for k in tournament_individuals_opponents if
                                      k not in tournament_opponents]
            rest_of_the_population = random.sample(rest_of_the_population, even_num - tournament)
            logging.info('number of rest of population to select after tournament= =', len(rest_of_the_population))
            tournament_winners = []
            for i in range(0, len(tournament_individuals)):
                if self.population_fitness[tournament_individuals[i]] > \
                        self.population_fitness[tournament_individuals_opponents[i]]:
                    tournament_winners.append(tournament_individuals[i])
                else:
                    tournament_winners.append(tournament_individuals_opponents[i])
            self.population = elitism_population + tournament_winners + rest_of_the_population
            logging.info('number of population after tournament= =', len(self.population))

        elif selection_method == 2:  # ordered_best_score (elitism)
            self.population = list(self.population_fitness)[:even_num]
            logging.info('number of population after ordered best score= =', len(self.population))

        elif selection_method == 3:  # random_best_score (elitism may apply)
            rest_of_the_population = random.choices(other_population[0:even_num],
                                                    weights=other_population_fitness[0:even_num],
                                                    k=even_num)
            self.population = elitism_population + rest_of_the_population
            logging.info('number of population after random best score= =', len(self.population))
        else:
            raise ValueError(
                'pb with input parameters selection_method=' + selection_method + ', elitism=' + str(
                    elitism) + ', tournament=' + str(tournament))

        self.population_size = len(self.population)
        self.store_population_size.append(self.population_size)
        return self.population

    def crossover(self, crossover_method):
        for i in range(0, self.population_size - 1, 2):
            logging.info('parent1(' + str(len(self.population[i].gene)) + ')=' + str(self.population[i]))
            logging.info('parent2(' + str(len(self.population[i + 1].gene)) + ')=' + str(self.population[i + 1]))
            crossover = round(random.random() * self.population[i].size)
            child1, child2 = self.population[i].crossover(self.population[i + 1], crossover, crossover_method)
            self.population.append(child1)
            self.population.append(child2)
            logging.info('child1(' + str(len(child1.gene)) + ')=' + str(child1))
            logging.info('child2(' + str(len(child2.gene)) + ')=' + str(child2))
        self.population_fitness = None
        self.population_size = len(self.population)
        logging.info('number of population after cross over= =', self.population_size)
        return self.population

    def mutation(self):
        mutation_nbr = round(self.mutation_tx * self.population_size)
        mutation_list_idx = random.sample(range(0, self.population_size), mutation_nbr)
        for idx in mutation_list_idx:
            self.population[idx].mutation()
        logging.info('number of population after mutation= =', len(self.population))
        return self.population

    def has_converged(self, scope) -> bool:
        logging.info('scope=' + str(scope))
        return eval(self.stop_condition)

    def __repr__(self) -> str:
        result = 'display population size ' + str(self.population_size) + '\n'
        for pop in self.population:
            result = result + str(pop)
            if self.population_fitness is not None:
                result = result + ' fitness=' + str(self.population_fitness[pop]) + ' score=' + str(pop.score())
            result = result + '\n'
        result = result + 'Problem complexity='+str(math.factorial(self.population[0].size - 1) / 2)+'\n'

        return result

    def display_fitness(self):
        fig, ax1 = plt.subplots()
        color = 'tab:red'
        ax1.set_xlabel('Population generation (s)')
        ax1.set_ylabel('Fitness', color=color)
        ax1.plot(self.store_average_fitness, color=color)
        ax1.tick_params(axis='y', labelcolor=color)

        # ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
        # color = 'tab:blue'
        # ax2.set_ylabel('population size', color=color)  # we already handled the x-label with ax1
        # ax2.plot(self.store_population_size, color=color)
        # ax2.tick_params(axis='y', labelcolor=color)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.show()

    def display_score(self):
        fig, ax1 = plt.subplots()
        color = 'tab:red'
        ax1.set_xlabel('Population generation (s)')
        ax1.set_ylabel('score', color=color)
        ax1.set_title('best score =' + str(self.store_best_score[-1]))
        ax1.plot(self.store_best_score, color=color)
        ax1.tick_params(axis='y', labelcolor=color)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.show()
