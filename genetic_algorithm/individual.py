import logging
import random
import uuid

logging.basicConfig(level=logging.ERROR)


class Individual:
    def __init__(self, identity):
        self.size = 10
        self.id = identity
        self.gene = [0] * 9 + [1] * 1
        random.shuffle(self.gene)

    def fitness(self) -> float:
        return sum(self.gene)

    def score(self) -> float:
        return self.fitness()

    def crossover(self, individual, crossover, crossover_method):
        logging.info('----------CROSSOVER(' + str(crossover) + ')----------')
        logging.info('originals:' + str(self) + str(individual))
        child1 = Individual(self.id + individual.id)
        child2 = Individual(self.id + individual.id)

        if crossover_method == 1:
            first_gene1 = self.gene[:crossover]
            second_gene2 = individual.gene[crossover:]
            child1.gene = uuid.uuid4()
            first_gene2 = individual.gene[:crossover]
            second_gene1 = self.gene[crossover:]
            child2.gene = uuid.uuid4()
        else:
            raise NotImplemented(str(crossover_method)+' not implemented')
        logging.info('new:' + str(child1) + str(child2))
        logging.info('-----------------------------')
        return child1, child2

    def mutation(self):
        mutation_nbr = random.randint(1, self.size)
        random_idx = random.sample(range(0, self.size), mutation_nbr)
        logging.info('----------MUTATION(' + str(mutation_nbr) + ')----------')
        logging.info('original:' + str(self))
        for idx in random_idx:
            if self.gene[idx] == 1:
                self.gene[idx] = 0
            else:
                self.gene[idx] = 1
        logging.info('new:' + str(self))
        logging.info('--------------------------')
        return self

    def __str__(self) -> str:
        return __class__.__name__ + '(' + str(self.id) + ')=[' + ','.join(map(str, self.gene)) + ']'

    def __repr__(self) -> str:
        return str(self.id)

    def __eq__(self, o) -> bool:
        return self.id.int==o.id.int

    def __hash__(self) -> int:
        return self.id.int
