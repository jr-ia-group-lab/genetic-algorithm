import math
import uuid

from Travelling_Salesman import Travel
from genetic_algorithm import GaManagement
from genetic_algorithm.GAManagement import selection_method_dict, crossover_method_dict

if __name__ == '__main__':
    # initial_population = [Individual(i) for i in range(0, 50)]
    initial_population = [Travel(uuid.uuid4()) for i in range(0, 170)]

    ga = GaManagement(selection_tx=0.5, mutation_tx=0.15, stop_condition=compile('scope==170', "<string>", 'eval'))
    ga.initial_population(initial_population)
    ga.calculate_fitness_population()
    print(ga)
    ga.population[0].display_map(graphic='Random ' + str(int(ga.population[0].score())) + 'km')

    i = 0
    while not (ga.has_converged(i)):
        ga.select_population(selection_method=selection_method_dict['ordered_best_score'], elitism=0, tournament=0)
        ga.crossover(crossover_method_dict['double_point'])
        ga.mutation()
        ga.calculate_fitness_population()
        i = i + 1
    ga.select_population(selection_method=selection_method_dict['ordered_best_score'], elitism=0, tournament=0)
    print(ga)
    ga.display_fitness()
    ga.display_score()
    ga.population[0].display_map(graphic='Genetic Algo ' + str(int(ga.store_best_score[-1])) + 'km')
