#### GENETIC ALGORITHM

_Use Case : Salesman Traveler_

main.py : create a population of 170 travel individuals with 22 Towns (complexity = 0,5 * 21! = 2.55e+19)<br>
Runs generations until GAManagement stop condition is True<br>
For each generation :
<ul>
<li>Select 0.5% of the population using the elitism method (Tournament and random have also been implemented) and kills the other individuals</li>
<li>Crossover population by mixing towns of the 2 parent travels and generate 2 children Travels (double point and single point have been implemented)</li>
<li>Mutate randomly 0.5 of the population by switching 2 towns within individual Travels</li>
<li>Calculate the fitness of the population corresponding of the score length of the global Travel between towns</li>
</ul>
The population remains at 170 Travels

PVC.py : same exercise using simulated annealing 

