# -*- coding: utf-8 -*-
# programme de résolution du problème du voyaguer de commerce
# par l'algorithme du recuit simulé
# Dominique Lefebvre pour TangenteX.com
# 14 mars 2017

# importation des librairies

from matplotlib.pyplot import *
from numpy import exp, arange, random
# from numpy import roll, c_, sqrt


from Travelling_Salesman import Travel, Town

travel_initial = Travel(1)
travel_initial.display_map(graphic='Random '+str(int(travel_initial.score())))
# paramètres du problème
N = travel_initial.size  # nombre de villes

# paramètres de l'algorithme de recuit simulé
T0 = 10.0
Tmin = 1e-2
tau = 1e4


# fonction de calcul de l'énergie du système, égale à la distance totale
# du trajet selon le chemin courant
def energietotale():
    global trajet
    energie = 0.0
    # coord = c_[x[trajet], y[trajet]]
    # energie = sum(sqrt(np.sum((coord - roll(coord, -1, axis=0)) ** 2, axis=1)))
    for tnum, tid in enumerate(trajet):
        town1 = Town(x[tid], y[tid], str(tid))
        if tnum == len(trajet) - 1:
            town2 = Town(x[trajet[0]], y[trajet[0]], str(trajet[0]))
        else:
            town2 = Town(x[trajet[tnum + 1]], y[trajet[tnum + 1]], str(trajet[tnum + 1]))
        energie = energie + town1.distance(town2)

    return energie


# fonction de fluctuation autour de l'état "thermique" du système
def fluctuation(fi, fj):
    global trajet
    minimum = min(fi, fj)
    maximum = max(fi, fj)
    trajet[minimum:maximum] = trajet[minimum:maximum].copy()[::-1]
    return


# fonction d'implémentation de l'algorithme de Metropolis
def metropolis(e1, e2):
    global T
    if e1 <= e2:
        e2 = e1  # énergie du nouvel état = énergie système
    else:
        de = e1 - e2
        if random.uniform() > exp(-de / T):  # la fluctuation est retenue avec
            fluctuation(i, j)  # la proba p. sinon retour trajet antérieur
        else:
            e2 = e1  # la fluctuation est retenue
    return e2


# initialisation des listes d'historique
Henergie = []  # énergie
Htemps = []  # temps
HT = []  # température

# répartition aléatoire des N villes sur le plan [0..1,0..1]
x = np.asarray([t.lon for t in travel_initial.gene])
# x = random.uniform(size=N)
y = np.asarray([t.lat for t in travel_initial.gene])
# y = random.uniform(size=N)

# définition du trajet initial : ordre croissant des villes
trajet = arange(N)
trajet_init = trajet.copy()

# calcul de l'énergie initiale du système (la distance initiale à minimiser)
ec = energietotale()

# boucle principale de l'algorithme de recuit simulé
t = 0
T = T0
while T > Tmin:
    # choix de deux villes différentes au hasard
    i = random.randint(0, N - 1)
    j = random.randint(0, N - 1)
    if i == j:
        continue

    # création de la fluctuation et mesure de l'énergie
    fluctuation(i, j)
    ef = energietotale()
    ec = metropolis(ef, ec)

    # application de la loi de refroidissement    
    t += 1
    T = T0 * exp(-t / tau)

    # historisation des données
    if t % 10 == 0:
        Henergie.append(ec)
        Htemps.append(t)
        HT.append(T)

print('Energie=', Henergie[-1])
travel_final = Travel(2)
travel_final.gene = [travel_initial.gene[i] for i in trajet]
travel_final.display_map(graphic='Simulated annealing ' +str(int(travel_final.score())))

# fin de boucle - affichage des états finaux
# affichage du réseau
# fig1 = figure(1)
# subplot(1, 2, 1)
# xticks([])
# yticks([])
# plot(x[trajet_init], y[trajet_init], 'k')
# plot([x[trajet_init[-1]], x[trajet_init[0]]], [y[trajet_init[-1]], y[trajet_init[0]]], 'k')
# plot(x, y, 'ro')
# title('Trajet initial')
#
# subplot(1, 2, 2)
# xticks([])
# yticks([])
# plot(x[trajet], y[trajet], 'k')
# plot([x[trajet[-1]], x[trajet[0]]], [y[trajet[-1]], y[trajet[0]]], 'k')
# plot(x, y, 'ro')
# title('Trajet optimise')
# show()

# affichage des courbes d'évolution
fig2 = figure(2)
subplot(1, 2, 1)
semilogy(Htemps, Henergie)
title("Evolution de l'energie totale du systeme")
xlabel('Temps')
ylabel('Energie')
subplot(1, 2, 2)
semilogy(Htemps, HT)
title('Evolution de la temperature du systeme')
xlabel('Temps')
ylabel('Temperature')
# show()
