import logging
import math
import random
import uuid
import matplotlib.pyplot as plt

from genetic_algorithm import Individual


class Town:
    def __init__(self, lon, lat, name):
        self.lon = lon
        self.lat = lat
        self.name = name

    def distance(self, town):
        distancex = (town.lon - self.lon) * 40000 * math.cos((self.lat + town.lat) * math.pi / 360) / 360
        distancey = (self.lat - town.lat) * 40000 / 360
        distance = math.sqrt((distancex * distancex) + (distancey * distancey))
        return distance

    def __eq__(self, o) -> bool:
        if isinstance(o, Town):
            return self.name == o.name
        return NotImplemented

    def __str__(self) -> str:
        return self.name + '(' + str(self.lon) + ',' + str(self.lat) + ')'


class Travel(Individual):
    def __init__(self, identity):
        super().__init__(identity)
        self.gene = []
        self.gene.append(Town(3.002556, 45.846117, 'Clermont-Ferrand'))
        self.gene.append(Town(-0.644905, 44.896839, 'Bordeaux'))
        self.gene.append(Town(-1.380989, 43.470961, 'Bayonne'))
        self.gene.append(Town(1.376579, 43.662010, 'Toulouse'))
        self.gene.append(Town(5.337151, 43.327276, 'Marseille'))
        self.gene.append(Town(7.265252, 43.745404, 'Nice'))
        self.gene.append(Town(-1.650154, 47.385427, 'Nantes'))
        self.gene.append(Town(-1.430427, 48.197310, 'Rennes'))
        self.gene.append(Town(2.414787, 48.953260, 'Paris'))
        self.gene.append(Town(3.090447, 50.612962, 'Lille'))
        self.gene.append(Town(5.013054, 47.370547, 'Dijon'))
        self.gene.append(Town(4.793327, 44.990153, 'Valence'))
        self.gene.append(Town(2.447746, 44.966838, 'Aurillac'))
        self.gene.append(Town(1.750115, 47.980822, 'Orleans'))
        self.gene.append(Town(4.134148, 49.323421, 'Reims'))
        self.gene.append(Town(7.506950, 48.580332, 'Strasbourg'))
        self.gene.append(Town(1.233757, 45.865246, 'Limoges'))
        self.gene.append(Town(4.047255, 48.370925, 'Troyes'))
        self.gene.append(Town(0.103163, 49.532415, 'Le Havre'))
        self.gene.append(Town(-1.495348, 49.667704, 'Cherbourg'))
        self.gene.append(Town(-4.494615, 48.447500, 'Brest'))
        self.gene.append(Town(-0.457140, 46.373545, 'Niort'))
        random.shuffle(self.gene)
        self.size = len(self.gene)

    def fitness(self) -> float:
        travel_distance = 0
        for idx in range(0, self.size):
            main_town = self.gene[idx]
            if idx + 1 < self.size:
                arrival_town = self.gene[idx + 1]
            else:
                arrival_town = self.gene[0]
            travel_distance += main_town.distance(arrival_town)
        return 1 / float(travel_distance)

    def score(self) -> float:
        return 1 / self.fitness()

    def crossover(self, individual, crossover, crossover_method):
        child1 = Travel(uuid.uuid4())
        child2 = Travel(uuid.uuid4())
        if crossover_method == 2:
            # 2 points methods
            crossover = sorted(random.sample(range(0, self.size), 2))

            interval_from_parent1 = [t for i, t in enumerate(self.gene) if i > crossover[0] or i < crossover[1]]
            logging.info('from_parent1=', interval_from_parent1)
            other_from_parent2 = [t for t in individual.gene if t not in interval_from_parent1]
            logging.info('from_parent2=', other_from_parent2)
            child1.gene = other_from_parent2[:crossover[0]] + interval_from_parent1 + other_from_parent2[crossover[1]:]

            interval_from_parent2 = [t for i, t in enumerate(individual.gene) if i > crossover[0] or i < crossover[1]]
            logging.info('from_parent2=', interval_from_parent2)
            other_from_parent1 = [t for t in self.gene if t not in interval_from_parent2]
            logging.info('from_parent1=', other_from_parent1)
            child2.gene = other_from_parent1[:crossover[0]] + interval_from_parent2 + other_from_parent1[crossover[1]:]
        else:
            raise NotImplemented(str(crossover_method) + ' not implemented')
        return child1, child2

    def mutation(self):
        # switch to town of gene
        nums = sorted(random.sample(range(0, self.size), 2))
        logging.info('before mutation ' + str(self))
        logging.info('switch between ' + str(nums[0]) + ' and ' + str(nums[1]))
        t = self.gene[nums[0]]
        self.gene[nums[0]] = self.gene[nums[1]]
        self.gene[nums[1]] = t
        logging.info('after mutation ' + str(self))
        return self

    def __str__(self) -> str:
        return super().__str__()

    def __repr__(self) -> str:
        return super().__repr__()

    def display_map(self, graphic=None):
        cumul = 0
        if graphic is not None:
            fig, ax1 = plt.subplots()
            ax1.set_xlabel('Longitute')
            ax1.set_ylabel('Latitude')
            ax1.set_title('Map ' + graphic)

        for i, t in enumerate(self.gene):
            if i + 1 == self.size:
                cumul = cumul + t.distance(self.gene[0])
                print(t, '->', self.gene[0], ' distance=', t.distance(self.gene[0]), ' cumul=', cumul)
                plt.plot([t.lon, self.gene[0].lon], [t.lat, self.gene[0].lat], marker='h')
            else:
                cumul = cumul + t.distance(self.gene[i + 1])
                print(t, '->', self.gene[i + 1], ' distance=', t.distance(self.gene[i + 1]), ' cumul=', cumul)
                plt.plot([t.lon, self.gene[i + 1].lon], [t.lat, self.gene[i + 1].lat], marker='h')
        if graphic is not None:
            plt.show()
